package de.hpi.javaide.breakout.elements;

import static de.hpi.javaide.breakout.starter.GameConstants.MAX_BALLS;
import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_X;
import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_Y;
import static de.hpi.javaide.breakout.starter.GameConstants.STARTPOSITION;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import de.hpi.javaide.breakout.Displayable;
import de.hpi.javaide.breakout.Measureable;
import de.hpi.javaide.breakout.starter.Game;

//     hier werden wir sicher eine Collection brauchen um die Bälle unterzubringen.
//     Vermutlich werden wir wissen wollen wann das Depot leer ist.
//     Irgendwie müssen die Bälle an den Start gebracht werden.
public class BallDepot implements Displayable, Measureable {
	
	static List<Ball> balls = new ArrayList<>();

	//private static Game game;

	private static int extraBalls = 0;
	
	private static final int SPACE_BETWEEN_THE_BALLS = 5;
	private static final int DEPOT_POS_X = SCREEN_X * 85 / 100;
	private static final int DEPOT_POS_Y = SCREEN_Y * 1 / 10;
	
	public BallDepot(Game game) {
		Ball ball;
		for ( int i = 0; i < MAX_BALLS; i++){
			ball = new Ball(game, new Point(DEPOT_POS_X, DEPOT_POS_Y));
			ball.setX(ball.getX() + (SPACE_BETWEEN_THE_BALLS + ball.getWidth()) * i);
			balls.add(ball);
		}
	}

	@Override
	public int getX() {
		return DEPOT_POS_X;
	}

	@Override
	public int getY() {
		return DEPOT_POS_Y;
	}

	@Override
	public int getWidth() {   // this method is not used in breakout actual
		return 0;
	}

	@Override
	public int getHeight() {  // this method is not used in breakout actual
		return 0;
	}

	@Override
	public void display() {
		balls.forEach(ball -> ball.display());
	}

	public boolean isEmpty() {
		return balls.isEmpty();
	}

	public Ball dispense() {
		Ball ball;
		if(!isEmpty()){
			ball = balls.remove(0); 
			ball.setPos(STARTPOSITION); 
			ball.getSpeed();
			ball.ranomizeSpeed();
			return ball;
		}
		return null;
	}

	public static void add(Game game){
		Ball ball = new Ball(game, new Point(DEPOT_POS_X, DEPOT_POS_Y));
		ball.setX(ball.getX() + (SPACE_BETWEEN_THE_BALLS + ball.getWidth()) - (SPACE_BETWEEN_THE_BALLS * ++extraBalls));
		balls.add(ball);
	}
}
