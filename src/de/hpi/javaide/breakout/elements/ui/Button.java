package de.hpi.javaide.breakout.elements.ui;

import java.awt.Point;
import java.awt.Rectangle;

import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.starter.Game;
import processing.core.PApplet;

public class Button {
	
	private Game game;
	
	private String name;
	private Rectangle rectangle;

	private String text;

	public Button(String name, Rectangle rectangle) {
		this.name = name;
		this.rectangle = rectangle;
	}
	
	public Button(Game game, String name, String text, Rectangle rectangle){
		this(name, rectangle);
		this.game = game;
		this.text = text;
	}

	public void show(){
		if(game == null) return;
		game.rectMode(PApplet.RECT);
		game.noStroke();
		game.fill(100,100,100);
		int x = rectangle.x;
		int y = rectangle.y;
		int w = rectangle.width;
		int h = rectangle.height;
		game.rect(x, y, w, h);
		game.textFont(Font.getFont16());
		game.textAlign(PApplet.CENTER, PApplet.CENTER);
		game.fill(200,200,200);
		game.text(text, x, y);
	}
	
	public String getName() {
		return name;
	}

	public boolean contains(Point mousePosition) {
		return rectangle.contains(mousePosition);
	}
	
	
}
