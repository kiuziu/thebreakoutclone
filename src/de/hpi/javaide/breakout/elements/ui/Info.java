package de.hpi.javaide.breakout.elements.ui;

import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.starter.Game;

//     nach dem die fehlenden Methoden ergänzt wurden, muss hier noch ein Konstruktorparameter 
//     an das zugehörige Attribut übergeben werden.

public class Info extends UIObject {

	private String content;
	
	public Info(Game game, String content) {
		super(game);
		this.content = content;
	}

	@Override
	public void display() {
	    game.textFont(Font.getFont24());
	    game.text(content, game.width / 4, game.height / 3);
	}

	@Override
	public void update(String input) {
		content = input;
		display();
		
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}
}
