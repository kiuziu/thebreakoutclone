package de.hpi.javaide.breakout.elements.ui;

import javax.swing.JOptionPane;

import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.starter.Game;
import processing.core.PApplet;

public class Score extends UIObject {
	
	private UIObject infoBox;

	private int score;
	private int maxScore;
	private String scoreDetails = "";
	
	private int yPos;
	
    private String[] highscoreList;
    public String[] actualScores = {"Player","0","0"};
    

	public Score(Game game) {
		super(game);
		score = 0;
		maxScore = 0;
		yPos = game.height * 7 / 100; // Score y-position on the Screen
		highscoreList = loadHighscore();
	}

    @Override
    public void update(String input) {
    	int actualScore = Integer.parseInt(input);
    	if(actualScore > maxScore) maxScore = actualScore;
        score += actualScore;
        actualScores[1] = "" + score;
        actualScores[2] = "" + maxScore;
    }

	@Override
    public void display() {
        String player = actualScores[0];
        String playersScore = "'s Score: " + score + "\n";
        playersScore += "highest hit: " + maxScore;  
        game.fill(255);
        game.textFont(Font.getFont16());
        game.textAlign(PApplet.CENTER);
        game.text(player + playersScore, game.width / 6, yPos);
        game.textFont(Font.getFont16(), 12);
        game.text(buildHighscoreRow(highscoreList[0]), game.width / 2, yPos);
        game.textFont(Font.getFont16(), 12);
        game.textAlign(PApplet.LEFT);
        game.text(scoreDetails, game.width / 100, game.height - 200);
        game.textAlign(PApplet.CENTER, PApplet.CENTER);
    }

	public String buildHighscoreList() {
		String result = "";
		for(String s: highscoreList) {
        	result += buildHighscoreRow(s);	                                        
        }
		return result;
	}
	
	private String buildHighscoreRow(String s) {
		String[] data = s.split(";");
		if(!data[0].equals("-")){
			return data[0] + "'s Highscore: " 
		         + data[1] + " - highest hit: " 
				 + data[2] + "\n";
		}
		return "";
	}

	@Override
    public void reset() {
    	score = 0;
    	maxScore = 0;
    	actualScores[1] = "" + 0;
    	actualScores[2] = "" + 0;
    }

	private String[] loadHighscore() {
		String[] loadResult;
		String path = System.getProperty("user.dir");
		loadResult = game.loadStrings(path + "/highscore.txt");
		if(loadResult == null){
			loadResult = makeHighschoreList();
		}
        return loadResult;
	}
	
    private String[] makeHighschoreList() {
		String[] highscoreList = {
				"Player;0;0",
				"Player;0;0",
				"Player;0;0",
				"Player;0;0",
				"Player;0;0",
				"Player;0;0",
				"Player;0;0",
				"Player;0;0",
				"Player;0;0",
				"Player;0;0"};
		this.highscoreList = highscoreList;
		saveHighscore();
		return highscoreList;
	}

	public void saveHighscore() {
		if(game.demoMode) return;
        String path = System.getProperty("user.dir");
        game.saveStrings(path + "/highscore.txt", highscoreList);
    }
	
	private String getPlayerName() {
	    String inputValue;
	    do{
	        inputValue = JOptionPane.showInputDialog("Please, enter your name");
	    }while(inputValue == null || inputValue.isEmpty());
	    inputValue = inputValue.trim().toLowerCase();
	    inputValue = inputValue.substring(0, 1).toUpperCase() + inputValue.substring(1);
	    return inputValue;
	}
	
	public void setPlayersName() {
		game.score.actualScores[0] = game.score.getPlayerName();
	}

	public boolean isNewHighscore() {
		int posInHighscore = -1;
		int pos = 0;
		for(String s:highscoreList) {
			String data[] = s.split(";");
			int actualScore = Integer.parseInt(actualScores[1]);
			int storedHighscore = Integer.parseInt(data[1]);
			if(actualScore > storedHighscore) {
				posInHighscore = pos;
				break;
			}
			pos++;
		}
		if(posInHighscore > -1) {
			insertActualScoreInHighscore(posInHighscore);
			return true;
		}
		return false;
	}

	private void insertActualScoreInHighscore(int position) {
		String actScore = actualScores[0] + ";" + actualScores[1] + ";" + actualScores[2]; 
		String[] temp = new String[highscoreList.length];
		for(int i = 0; i < position; i++){
			temp[i] = highscoreList[i];
		}
		temp[position] = actScore;
		for(int i = position; i < highscoreList.length-1; i++){ // durch -1 wird der letzte Eintrag aus der Liste entfernt
			temp[i+1] = highscoreList[i];
		}
		highscoreList = temp;
	}

	public void updateHighscore() {
		if(isNewHighscore()) saveHighscore();	
	}

	public void displayHighscoreList() {
		String lastHighscore = "[H] G I H S C O R E\n";
        lastHighscore += buildHighscoreList();    
		infoBox = new InfoBox(game, lastHighscore);
		infoBox.display();
	}

	public void setScoreDetails(String s) {
		this.scoreDetails = s;	
	}
	
}
