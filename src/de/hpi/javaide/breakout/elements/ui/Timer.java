package de.hpi.javaide.breakout.elements.ui;

import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_X;
import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_Y;
import static de.hpi.javaide.breakout.starter.GameConstants.*;

import de.hpi.javaide.breakout.SpecialBrick;
import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.starter.Game;
import processing.core.PApplet;

public class Timer extends UIObject {
	
	private int time;
	private int addTime;
	private boolean displayOn;
	private int seconds;

	private SpecialBrick sb;

	public boolean isDisplayOn() {
		return displayOn;
	}

	public Timer(Game game) {
		super(game);
	}

	private int getActualtimeInSeconds() {
		return PApplet.second() + PApplet.minute() * 60 + PApplet.hour() * 3600;
	}

	@Override
	public void display() {
		if(isDisplayOn()){
			game.fill(255);
			game.textFont(Font.getFont16());
			time = this.addTime - getActualtimeInSeconds();
			int x = SCREEN_X - 150;
			int y = SCREEN_Y - 30 * sb.ordinal();
			game.text(sb.getText() + ": ", x, y);
			game.arc(x + 110, y, 30, 30, (PI_EINHALB) - ((ZWEI_PI) / (seconds+0.1f) * time), (PI_EINHALB), PApplet.PIE);			
			if(time <= 0) {
				displayOn = false;
			}
		}
	}

	@Override
	public void update(String input) {
		display();
	}

	@Override
	public void reset() {
		// not used 
	}

	public void start(int seconds, SpecialBrick sb) {
		this.sb = sb;
		this.seconds = seconds;
		addTime = seconds + getActualtimeInSeconds();
		displayOn = true;
	}
}
