package de.hpi.javaide.breakout.elements.ui;

import java.awt.Point;

import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.starter.Game;
import de.hpi.javaide.breakout.starter.GameConstants;
import processing.core.PApplet;

public class InfoBox extends UIObject{

	private Point position;
	private String text;
	private int width;
	private int height;
	private int gap = 20;

	public InfoBox(Game game, String text, int x, int y) {
		super(game);
		this.position = new Point(x, y);
		this.text = text;		
		int widthestLine = widthestLine(text);
		int numberOfLines = PApplet.split(text, "\n").length;
		width = widthestLine * 10 + gap;
		height = numberOfLines * 30 + gap;
	}
	
	public InfoBox(Game game, String text){
		this(game, text, GameConstants.SCREEN_X / 2, GameConstants.SCREEN_Y / 2);
	}

	private int widthestLine(String text) {
		if(text == null || text.isEmpty()) {
			throw new IllegalArgumentException("Argument is empty or null. That not allowed!");
		}
		String[] lines = PApplet.split(text, "\n");
		int max = 0;
		for(String s : lines){
			if(max < s.length()){
				max = s.length();
			}
		}
		return max;
	}

	@Override
	public void display() {
		game.rectMode(PApplet.CENTER);
		game.stroke(0, 155, 0);
		game.fill(60, 210, 210);
		game.rect(position.x, position.y, width, height, 20);
		game.fill(230, 255,255);
		game.textFont(Font.getFont16());
		game.textAlign(PApplet.CENTER, PApplet.CENTER);
		game.text(text, position.x, position.y, width - gap, height - gap);
		
	}

	@Override
	public void update(String input) {
		this.text = input;
		display();
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}
