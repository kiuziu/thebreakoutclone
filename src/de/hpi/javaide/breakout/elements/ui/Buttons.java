package de.hpi.javaide.breakout.elements.ui;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import de.hpi.javaide.breakout.starter.Game;

public class Buttons {
	
	private static ArrayList<Button> buttons = new ArrayList<>();
	
	private Buttons(){}

	public static void add(String name, Rectangle rectangle) {
		Button button = new Button(name, rectangle);
		buttons.add(button);		
	}

	public static void add(Game game, String name, String text, Rectangle rectangle) {
		Button button = new Button(game, name, text, rectangle);
		buttons.add(button);		
	}
	
	public static String clicked(Point mousePosition) {
		for(Button b: buttons){
			if(b.contains(mousePosition)){
				b.show();
				return b.getName();
			}
		}
		return "NOTHING";
	}

}
