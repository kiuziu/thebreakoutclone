package de.hpi.javaide.breakout.elements;

import static de.hpi.javaide.breakout.starter.GameConstants.BALL_DIAMETER;

import java.awt.Dimension;
import java.awt.Point;

import de.hpi.javaide.breakout.SpecialBrick;
import de.hpi.javaide.breakout.basics.Elliptic;
import de.hpi.javaide.breakout.basics.Vector;
import de.hpi.javaide.breakout.elements.ui.Timer;
import de.hpi.javaide.breakout.starter.Game;
import processing.core.PApplet;

/**
 * Blueprint for a Ball
 * 
 * @author Ralf Teusner and Tom Staubitz
 *
 */
//     neben dem Ergänzen der vom Interface erwarteten Methoden, 
//     sollte der Ball Eigenschaften wie Größe und Richtung mitbringen.
//     Richtung wird in der Regel als Vector definiert. 
//     Vermutlich sollte er die Richtung ändern können und sehr wahrscheinlich wird früher oder später 
//     jemand wissen wollen in welche Richtung er fliegt.
public class Ball extends Elliptic {

	private static Dimension dimension = new Dimension(BALL_DIAMETER, BALL_DIAMETER);
	
	private Vector speed;

	private boolean alive;

	private Timer timer;
	
	public Ball(Game game, Point position) {
		super(game, position, dimension);
		speed = new Vector(6.0f, 6.0f);
		alive = true;
	}

	@Override
	public void display() {
		if(timer != null && timer.isDisplayOn()) {
			timer.display();
		}else{
			normalizeSize();
		}
		game.ellipseMode(PApplet.CENTER);
		game.stroke(0,0,0,155);
		game.fill((speed.absSum() * 6), 255 - (speed.absSum() * 6), 255 - (speed.absSum() * 6));	
		game.ellipse(getX(), getY(), getWidth(), getHeight());
	}

	public void move() {	
		int newX = Math.round((getX() + speed.getX()));
		int newY = Math.round((getY() + speed.getY()));
		update(new Point(newX, newY), new Dimension(getWidth(), getHeight()));
	}

	public void computeSpeedForX(int distanceToTheMiddleOfThePaddle) {
		speed.setX(speed.getX() + distanceToTheMiddleOfThePaddle / 6);
	}
	
	public void ranomizeSpeed() {
		speed = new Vector((float) (Math.random() * 12 - 6), 6.0f);
		correctSpeed();
	}
	
	public void setX(int x) {
		this.position.x = x;
	}

	public void setY(int y) {
		this.position.y = y;
	}
	
	public void setPos(Point point){
		this.position = point;
	}
	
	public void changeXDirection(){
		speed.setX( - speed.getX());
	}
	
	public void changeYDirection(){
		speed.setY( - speed.getY());
	}

	public boolean isAlive() {
		return this.alive;
	}

	public void lost() {
		if(alive){
			alive = !alive;
		}
	}

	public int getYDirection() {
		if(speed.getY() < 0) return -1;
		return 1;
	}
	
	public int getXDirection() {
		if(speed.getX() < 0) return -1;
		return 1;
	}
	
	public Vector getSpeed(){
		return speed;
	}

	public void setSpeed(float mult, SpecialBrick sb) {
		speed.mult(mult);
		correctSpeed();
		timer = new Timer(game);
		timer.start(20, sb);
	}

	private void correctSpeed() {
		if(Math.abs(speed.getX()) < 1.0f){
			speed.setX(1.0f);
		}
		if(Math.abs(speed.getY()) < 1.0f){
			speed.setY(1.0f);
		}
	}
	
	public void speedUp() {
		speed.mult(1.05f);	
	}
	
	private void normalizeSize() {
		Point position = new Point(getX(), getY());
		update(position, dimension);		
	}

	public void changeSize(float factor, SpecialBrick sb) {
		Point position = new Point(getX(), getY());
		Dimension dimension = new Dimension((int)(getWidth() * factor), (int)(getHeight() * factor));
		update(position, dimension);
		timer = new Timer(game);
		timer.start(20, sb);
	}
	
	public boolean touched(Paddle paddle) {
		boolean ballBetweenPaddleEdges = getX() >= paddle.xLeft() && getX() <= paddle.xRight();
		return yBottom() >= paddle.yTop() && ballBetweenPaddleEdges;
	}

	public boolean touched(Brick brick) {
		if(!brick.isAlive() || !isAlive()) return false;
    	boolean touched = ((yBottom() > brick.yTop()) && (yTop() < brick.yBottom())
                && (xRight() > brick.xLeft()) && (xLeft() < brick.xRight()));
    	return touched;
	}
	
	public String toString(){
		return "x: " + this.getX() + ", y: " + this.getY() + ", w: " + this.getWidth() + ", h: " + this.getHeight(); 
	}
}
