package de.hpi.javaide.breakout.elements;

import static de.hpi.javaide.breakout.starter.GameConstants.BALL_DIAMETER;
import static de.hpi.javaide.breakout.starter.GameConstants.PADDLE_WIDTH;
import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_X;
import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_X_NULL;
import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_Y;
import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_Y_NULL;

import java.awt.Dimension;
import java.awt.Point;

import de.hpi.javaide.breakout.starter.Game;

public class CollisionLogic {

	/**
	 * The constructor of this class is private to make sure that it is only used as a static class.
	 * - it cannot be instantiated,
	 * - it cannot hold a state,
	 * - it contains only static methods
	 */
	private CollisionLogic() {}
	
	/**
	 * This method provides a way to determine if the ball collides with any of the collidable elements on the screen.
	 * Paddle, Bricks, ...
	 * 
	 * @param game
	 * @param ball
	 * @param paddle
	 * @param wall
	 */
	public static void checkCollision(Game game, Ball ball, Paddle paddle, Wall wall) {	

		if(game.demoMode){
			Point p = new Point(ball.getX(), paddle.getY());
			Dimension d = new Dimension(paddle.getWidth(), paddle.getHeight());
			paddle.update(p, d);
		}	
		if(hasContactWithTopSide(ball)){
			actionAfterContactWithTopSide(ball);
		}
		if(hasContactWithLeftRightSide(ball)){
			actionAfterContactWithLeftRightSide(ball);
		}	
		if(hasContactWithBottomSide(ball)){
			ball.lost();
		}
		if(ball.touched(paddle)){
			actionAfterPaddleContact(ball, paddle);
		}		
		wall.forEach(brick -> checkContact(game, brick, ball, paddle));	
	}

	private static boolean hasContactWithTopSide(Ball ball) {
		return ball.yTop() < SCREEN_Y_NULL;
	}

	private static void actionAfterContactWithTopSide(Ball ball) {                          
		ball.setY(ball.getHeight() / 2);
		ball.changeYDirection();  	
	}

	private static boolean hasContactWithLeftRightSide(Ball ball) {
		return ball.xLeft() < SCREEN_X_NULL || ball.xRight() > SCREEN_X;
	}
	
	private static void actionAfterContactWithLeftRightSide(Ball ball) {  
		if(ball.xLeft() < SCREEN_X_NULL){
			ball.setX(ball.getWidth() / 2);
		}else if(ball.xRight() > SCREEN_X){
			ball.setX(SCREEN_X - ball.getWidth() / 2);
		}
		ball.changeXDirection();
	}

	private static boolean hasContactWithBottomSide(Ball ball) {
		return ball.yBottom() > SCREEN_Y;
	}

	private static void actionAfterPaddleContact(Ball ball, Paddle paddle) {
		int distanceToTheMiddleOfThePaddle = (ball.getX() - paddle.getX());
		ball.computeSpeedForX(distanceToTheMiddleOfThePaddle);
		ball.changeYDirection();
		ball.setY(paddle.yTop() - (ball.getHeight() / 2));
		ball.speedUp();
	}

	private static void checkContact(Game game, Brick brick, Ball ball, Paddle paddle) {
		if(ball.touched(brick)){
			brick.aging();
			if(!brick.isAlive()){
				switch(brick.getSpecialBrick()){
					case BALL_EXPAND:{
						ball.changeSize(2.0f, brick.getSpecialBrick());
						break;
					}
					case BALL_SHRINK:{
						ball.changeSize(0.5f, brick.getSpecialBrick());
						break;
					}
					case EXTRA:{
						BallDepot.add(game);
						break;
					}
					case PADDLE_REDUCE:{
						paddle.changeSize(0.5f, brick.getSpecialBrick());
						break;
					}
					case PADDLE_WIDEN:{
						paddle.changeSize(2.0f, brick.getSpecialBrick());
						break;
					}
					case SPEED_DOWN:{
						ball.setSpeed(0.5f, brick.getSpecialBrick());
						break;
					}
					case SPEED_UP:{
						ball.setSpeed(1.5f, brick.getSpecialBrick());
						break;
					}
				default:
					break;
				};
			}
			calculateAndIncreaseScore(game, brick, ball, paddle);
			computeBallDirection(brick, ball);
		}
	}

	private static void calculateAndIncreaseScore(Game game, Brick brick, Ball ball, Paddle paddle) {
		float speedFactor = Math.abs(ball.getSpeed().getX() * ball.getSpeed().getY());
		float paddleFactor = (float)PADDLE_WIDTH / (float)paddle.getWidth();
		float ballFactor = (float)BALL_DIAMETER / (float)ball.getWidth();
		float lifeFactor = 4 - brick.getLifespan();
		int score = Math.round(lifeFactor * speedFactor * paddleFactor * ballFactor);	
		String s = String.format("Data from the last Hit%n"
				               + "Points: %7s%n"
				               + "Factor´s%n"
				               + "Life: %s%n"
				               + "Paddle: %s%n"
				               + "Ball: %s%n"
				               + "last Score : %s", 
				               Math.round(speedFactor * 10.0) / 10.0, 
				               lifeFactor, 
				               paddleFactor, 
				               ballFactor, 
				               score);
		game.score.setScoreDetails(s);
		game.increaseScore(score);
	}

	private static void computeBallDirection(Brick brick, Ball ball) {
	
		int leftRight = Math.abs(ball.xRight() - brick.xLeft());
		int rightLeft = Math.abs(ball.xLeft() - brick.xRight());
		int topBottom = Math.abs(ball.yBottom() - brick.yTop());
		int bottomTop = Math.abs(ball.yTop() - brick.yBottom());
		
		if(ball.getXDirection() == 1 && ball.getYDirection() == 1){
			if(topBottom < leftRight){
				ball.changeYDirection();
				ball.setY(brick.yTop() - (ball.getHeight() / 2));
			}else{
				ball.changeXDirection();
				ball.setX(brick.xLeft() - (ball.getWidth() / 2));	
			}
			return;
		}
		if(ball.getXDirection() == 1 && ball.getYDirection() == -1){
			if(bottomTop < leftRight){
				ball.changeYDirection();
				ball.setY(brick.yBottom() + (ball.getHeight() / 2));
			}else{
				ball.changeXDirection();
				ball.setX(brick.xLeft() - (ball.getWidth() / 2));
			}
			return;
		}
		if(ball.getXDirection() == -1 && ball.getYDirection() == 1){
			if(topBottom < rightLeft){
				ball.changeYDirection();
				ball.setY(brick.yTop() - (ball.getHeight() / 2));
			}else{
				ball.changeXDirection();
				ball.setX(brick.xRight() + (ball.getWidth() / 2));
			}
			return;
		}
		if(ball.getXDirection() == -1 && ball.getYDirection() == -1){
			if(bottomTop < rightLeft){
				ball.changeYDirection();
				ball.setY(brick.yBottom() + (ball.getHeight() / 2));
			}else{
				ball.changeXDirection();
				ball.setX(brick.xRight() + (ball.getWidth() / 2));
			}
			return;
		}
	}
}
