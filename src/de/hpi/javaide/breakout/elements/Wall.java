package de.hpi.javaide.breakout.elements;

import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_X;
import static de.hpi.javaide.breakout.starter.GameConstants.SCREEN_Y;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;

import de.hpi.javaide.breakout.Displayable;
import de.hpi.javaide.breakout.starter.Game;

/**
 * Blueprint for the Wall
 * 
 * @author Ralf Teusner and Tom Staubitz and Manuel Prochnow
 *
 */

public class Wall implements Displayable, Iterable<Brick> {
	
	/**
	 * Data structure to keep the Bricks
	 */
	private ArrayList<Brick> wall = new ArrayList<>();
	
	private Game game;
	
	private int gap;
	
	private int brickWidth = 100;
	private int brickHeight = 20;

	private int columns;
	private int rows;
	
	private int wallFrame;

	/**
	 * Constructor for a Wall with col x row bricks with gap between the brick
	 * @param game
	 * @param col
	 * @param row
	 * @param gap
	 */
	public Wall(Game game, int col, int row, int gap) {
		this.game = game;
		columns = col;
		rows = row;
		this.gap = gap;
		buildWall(this.game, columns, rows);
	}

	/**
	 * Constructor for a Wall with i x j bricks with zero gap between the brick
	 * @param game
	 * @param col
	 * @param row
	 */
	public Wall(Game game, int col, int row) {
		this(game, col, row, 0);
	}
	
	/**
	 * Construct a random Wall with 3-7 columns x 3-7 rows of bricks with 5-25 gap between the bricks
	 * @param game
	 * @return
	 */
	public static Wall randomWall(Game game) {
		Wall w;
		do{
			w = new Wall(game, ran(3,7), ran(3,7), ran(5,25));
		}while(w == null);		
		return w;
	}

	private boolean fitsTheWallInTheScreen() {
		int wallWidth = brickWidth * columns + wallFrame; 
		int wallHeight = brickHeight * rows + wallFrame;
		return wallWidth < SCREEN_X && wallHeight < SCREEN_Y;
	}
	
	@Override
	public Iterator<Brick> iterator() {
		return wall.iterator();
	}
	
	/**
	 * Build the wall by putting the single bricks into their position
	 * Hint: You might want to use one or two for-loops
	 * 
	 * @param game
	 * @param columns
	 * @param rows
	 */
	private void buildWall(Game game, int columns, int rows) {
		brickWidth += gap;
		brickHeight += gap;
		wallFrame = (SCREEN_X - (brickWidth * columns)) / 2;
		if (fitsTheWallInTheScreen()){
			for(int y = 0; y < rows; y++){
				for(int x = 0; x < columns; x++){
					int xPos = wallFrame + (x * brickWidth) + (brickWidth / 2);
					int yPos = SCREEN_Y / 5 + (y * brickHeight) + (brickHeight / 2);
					Point point = new Point(xPos, yPos);
					Dimension dimension = new Dimension(brickWidth - gap, brickHeight - gap);
					Brick brick = new Brick(game, point, dimension);
					wall.add(brick);
				}
			}
		}else{
			Wall w = randomWall(game);
			wall = w.wall;
		}
	}
	
	/**
	 * check if wall is empty
	 * 
	 * @return true if the wall has no more bricks (wall is empty). false otherwise 
	 */
	public boolean isEmpty(){
		return wall.isEmpty();
	}
	/**
	 * return a Random Integer between min and max inclusive
	 * @param min int
	 * @param max int
	 * @return int
	 */
	private static int ran(int min, int max) {
		int a = (int) Math.round(Math.random() * (max - min));
		return a + min;
	}
	
	@Override
	public void display() {
		if(wall != null){
			update();
			wall.forEach(brick -> brick.display());
		}else{
			System.out.println("que iss'o ??");
		}
	}

	/**
	 * update the actually wall. 
	 * Remove all bricks that not alive.
	 * 
	 */
	private void update() {
		wall.removeIf(brick -> !brick.isAlive());		
	}	
}
