package de.hpi.javaide.breakout.elements;

import static de.hpi.javaide.breakout.starter.GameConstants.LIVES;

import java.awt.Dimension;
import java.awt.Point;

import de.hpi.javaide.breakout.SpecialBrick;
import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.basics.Rectangular;
import de.hpi.javaide.breakout.basics.Werkzeuge;
import de.hpi.javaide.breakout.starter.Game;
import processing.core.PApplet;

//	   wichtige Attribute: Größe, Position, Abstand der Bricks untereinander
//     Irgendwie muss ich herausbekommen ob der Stein noch existiert oder nicht.
public class Brick extends Rectangular {
	
	private int lifespan = LIVES;
	
	private SpecialBrick specialBrick;

	private boolean alive;
	
	public Brick(Game game, Point position, Dimension dimension) {
		super(game, position, dimension);
		setColor(255,200,100);
		alive = true;
		specialBrick = randomBrick();
	}
	
	/**
	 * 
	 * @return
	 */
	public SpecialBrick getSpecialBrick() {
		return specialBrick;
	}

	/**
	 * randomize Methode to make a brick to a special brick at the construction time
	 * @return randomly selected brick
	 */
	private SpecialBrick randomBrick() {
		return Werkzeuge.randomOfAny(SpecialBrick.NORMAL, Werkzeuge.randomOfAny(SpecialBrick.values()));
	}

	/**
	 * aging the brick. that means reduce attribute lifespan.
	 * change the brick color.
	 */
	public void aging(){
		if(alive){
			int newR = getB();
			int newG = getR();
			int newB = getG();
			setColor(newR, newG, newB);
			lifespan--;
			if(lifespan == 0){
				killBrick();
			}
		}
	}

	private void killBrick() {
			alive = false;
	}
	
	@Override
	public void display() {
		if(alive){
			game.rectMode(PApplet.CENTER);
			game.stroke(255,10,10);
			game.fill(getR(), getG(), getB());
			game.rect(getX(), getY(), getWidth(), getHeight());
			game.textFont(Font.getFont16(), 12);
			game.textAlign(PApplet.CENTER, PApplet.CENTER);
			game.fill(255-getR(),255-getG(),255-getB());
			game.text(specialBrick.getText(), getX(), getY());
		}
	}

	public boolean isAlive() {
		return alive;
	}
	
	public int getLifespan() {
		return lifespan;
	}
}
