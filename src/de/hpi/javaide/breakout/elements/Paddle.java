package de.hpi.javaide.breakout.elements;

import java.awt.Dimension;
import java.awt.Point;

import de.hpi.javaide.breakout.SpecialBrick;
import de.hpi.javaide.breakout.basics.Rectangular;
import de.hpi.javaide.breakout.elements.ui.Timer;
import de.hpi.javaide.breakout.starter.Game;
import static de.hpi.javaide.breakout.starter.GameConstants.*;
import processing.core.PApplet;

/**
 * Create the paddle
 * @param game Game provide access to the Processing features
 */
public class Paddle extends Rectangular {

	private Timer timer;

	public Paddle(Game game) {
		super(game, new Point(game.width / 2, game.height - 20), new Dimension(PADDLE_WIDTH, PADDLE_HIGHT));
		setColor(250, 100, 0);
	}

	@Override
	public void display() {
		if(timer != null && timer.isDisplayOn()) {
			timer.display();
		}else{
			normalizeSize();
		}
		game.rectMode(PApplet.CENTER);
		game.noStroke();
		game.fill(getR(), getG(), getB());
		game.rect(getX(), getY(), getWidth(), PADDLE_HIGHT);
	}

	private void normalizeSize() {
		Point position = new Point(getX(), getY());
		Dimension dimension =new Dimension(PADDLE_WIDTH, PADDLE_HIGHT);
		update(position, dimension);		
	}

	public void move() {
		position = new Point(game.mouseX, getY());
		dimension = new Dimension(getWidth(), PADDLE_HIGHT);
		update(position, dimension);		
	}
	
	public void changeSize(float factor, SpecialBrick sb) {		
		Point position = new Point(getX(), getY());
		Dimension dimension = new Dimension((int)(getWidth() * factor), PADDLE_HIGHT);
		update(position, dimension);
		timer = new Timer(game);
		timer.start(20, sb);
	}

}
