package de.hpi.javaide.breakout;

public enum SpecialBrick {
	
	NORMAL(""),
	EXTRA("Extraball"),
	PADDLE_WIDEN("widen Paddle"),
	PADDLE_REDUCE("reduce Paddle"),
	SPEED_UP("Ball faster"),
	SPEED_DOWN("Ball slower"),
	BALL_SHRINK("shrink Ball"),
	BALL_EXPAND("expand Ball");
	
	private String text;
	
	private SpecialBrick(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

}
