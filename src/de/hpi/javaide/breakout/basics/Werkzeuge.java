package de.hpi.javaide.breakout.basics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Werkzeuge {
	private Werkzeuge() {}

	public static final String EMPTY_STRING = "";
	
	
	public static String fillWith(String sequence, int times){
		sequence = sequence != null ? sequence:EMPTY_STRING;
		String ergebnis = sequence;
		for(int i = 1; i < times; i++) {
			ergebnis += sequence;
		}
		return ergebnis;
	}

	public static String textDateiLaden(File file) {
		String zeile, worte = EMPTY_STRING;

		BufferedReader f = null;
		try {
			f = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			while ((zeile = f.readLine()) != null) {
				worte += zeile + "\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return worte;
	}

	public static void warten(int milisekunden) {
		try {
			Thread.sleep(milisekunden);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static File pfadAuswahlIconsBilder(JFrame frame) {
		int auswahl;
		File meineDatei;
		do {
			JFileChooser meinDialog = new JFileChooser(System.getProperty("user.dir"));
			// -- Dateieinschraenkungen festlegen
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Bilder/Icons", "jpg", "png");
			meinDialog.setFileFilter(filter);
			auswahl = meinDialog.showOpenDialog(frame);
			if (auswahl == JFileChooser.APPROVE_OPTION) {
				meineDatei = meinDialog.getCurrentDirectory();
				return meineDatei;
			}
		} while (auswahl != JFileChooser.ERROR_OPTION);
		return null;
	}

	public static String[] findeDateien(String verzName, String suffix) {
		File verzeichnis = new File(verzName);
		if (verzeichnis.isDirectory()) {
			String[] alleDateien = verzeichnis.list();
			if (suffix == null) {
				return alleDateien;
			} else {
				List<String> selektiert = new ArrayList<String>();
				for (String dateiname : alleDateien) {
					if (dateiname.endsWith(suffix)) {
						selektiert.add(dateiname);
					}
				}
				return selektiert.toArray(new String[selektiert.size()]);
			}
		} else {
			System.out.println("Fehler: " + verzName + " muss ein Verzeichnis sein");
			return null;
		}
	}

	@SafeVarargs
	public static <T> T randomOfAny(T... x) {
		if (x == null || x.length == 0)
			throw new IllegalArgumentException("Illegal Argument!");
		return x[(int) (Math.random() * x.length)];
	}
	
	public static void main(String... args) {
		do {
			System.out.println("Eine von 6: " + randomOfAny(1, 2, 3, 4, 5, 6));
		} while (randomOfAny(1, 2, 3, 4, 5, 6, 7, 8, 9, 0) != 0);

		String[] str = { "man", "Gabi Loch", "Udo", "Manuel", "Elisa" };

		System.out.println(Arrays.toString(str));
		System.out.println(randomOfAny(str));
		System.out.println(fillWith("->", 5));
		System.out.println(randomOfAny("ja", "nein"));
	}
}