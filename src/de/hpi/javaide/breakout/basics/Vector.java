package de.hpi.javaide.breakout.basics;

import processing.core.PVector;

/**
 * Adapter to Processing's PVector
 * @author Ralf Teusner and Tom Staubitz and Manuel Prochnow
 *
 */
public class Vector {

	/**
	 * The PVector object to be wrapped
	 */
	private PVector vector;

	/**
	 * Constructor to set the x and the y value of the Vector
	 * @param x float
	 * @param y float
	 */
	public Vector(float x, float y) {
		vector = new PVector(x, y);
	}

	/**
	 * return  x-part of the vector
	 * @return
	 */
	public float getX() {
		return vector.x;
	}

	/**
	 * return y-part of the vector
	 * @return
	 */
	public float getY() {
		return vector.y;
	}

	/**
	 * set the x-part of the vector
	 * @param x new value
	 */
	public void setX(float x) {
		vector.x = x;
	}

	/**
	 * set the y-part of the vector
	 * @param y new value
	 */
	public void setY(float y) {
		vector.y = y;
	}
	
	/**
	 * Multiply the given Vector with the given number
	 * @param miltipicator
	 */
	public void mult(float miltipicator) {
		vector.mult(miltipicator);
	}
	
	/**
	 * Return sum of absolute values of the x-part and the y-part of the vector
	 * @return |x| + |y|
	 */
	public float absSum(){
		return Math.abs(getX()) + Math.abs(getY());
	}

	/**
	 * Normalize the wrapped vector
	 */
	public void normalize() {
		vector.normalize();
	}
}
