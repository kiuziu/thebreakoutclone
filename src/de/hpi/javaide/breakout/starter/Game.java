package de.hpi.javaide.breakout.starter;

import de.hpi.javaide.breakout.basics.Font;
import de.hpi.javaide.breakout.elements.ui.Score;
import de.hpi.javaide.breakout.screens.Screen;
import de.hpi.javaide.breakout.screens.ScreenManager;
import processing.core.PApplet;

public class Game extends PApplet implements GameConstants {

	private static final long serialVersionUID = 1L;

	/**
	 * global attributes for the highscore 
	 */
	public Score score;
	
	/**
	 * a switch to enter the demo mode
	 */
	public boolean demoMode = false;
	
	/**
	 *  Setup the game
	 */
	@Override
	public void setup() {
		size(SCREEN_X, SCREEN_Y);
		background(0);
		frameRate(30);
		Font.init(this);
		ScreenManager.setScreen(this, Screen.START);
		score = new Score(this);
	}

	/**
	 *  Update and draw everything in the game
	 */
	@Override
	public void draw() {
		background(150,200,100);
		ScreenManager.getCurrentScreen().update();
		ScreenManager.getCurrentScreen().display();
	}

	/**
	 *  Interact with the mouse
	 *  mouseMoved react without mouse click
	 */
	@Override
	public void mouseMoved() {
		ScreenManager.getCurrentScreen().handleMouseDragged();
	}

	/**
	 *  Interact with the mouse
	 *  mouseDragged react so with pressed mouse button
	 */
	@Override
	public void mouseDragged() {
		ScreenManager.getCurrentScreen().handleMouseDragged();
	}
	
	/**
	 * if mouse button pressed and mouse x/y is in a [button rectangle] do something
	 */
	@Override
	public void mousePressed() {
		// Toggle Comment for help rectangle position for initial Buttons.add() method
		// System.out.println(mouseX + "/" + mouseY);
		ScreenManager.getCurrentScreen().handleMouseButtonPressed();
		super.mousePressed();
	}

	/**
	 *  Interact with the keyboard for control the startscreen
	 *  
	 */
	@Override
	public void keyPressed() {
		switch (key) {
		case RETURN:
		case ENTER:
			ScreenManager.getCurrentScreen().handleKeyPressed(Screen.KEY_ENTER);
			break;
		case 'Q':
		case 'q':
			ScreenManager.getCurrentScreen().handleKeyPressed(Screen.KEY_QUIT);
			break;
		case 'N':
		case 'n':
			ScreenManager.getCurrentScreen().handleKeyPressed(Screen.KEY_NAME);
			break;
		case 'H':
		case 'h':
			ScreenManager.getCurrentScreen().handleKeyPressed(Screen.KEY_HGIHSCORE);
			break;
		case 'D':
		case 'd':
			ScreenManager.getCurrentScreen().handleKeyPressed(Screen.KEY_DEMO_MODE);
			break;
		default: 
			break;
		}
	}

	@Override
	public void keyReleased() {

	}

	public void increaseScore(int i) {
		ScreenManager.getCurrentScreen().increaseScore(i);
	}
}
