package de.hpi.javaide.breakout.starter;

import java.awt.Point;

public interface GameConstants {
	int SCREEN_X = 800;
	int SCREEN_Y = 600;
	
	int SCREEN_X_NULL = 0;
	int SCREEN_Y_NULL = 0;
	
	int LIVES = 3;
	
	int MAX_BALLS = 3;
	int BALL_DIAMETER = 20;
	
	int PADDLE_WIDTH = 100;
	int PADDLE_HIGHT = 20;

	Point STARTPOSITION = new Point(SCREEN_X/2, SCREEN_Y/2);
	
	float ZWEI_PI = (float) (2 * Math.PI);
	float PI = (float) Math.PI;
	float PI_HALBE = (float) (Math.PI / 2.0);
	float PI_VIETEL = (float) (Math.PI / 4.0);
	float PI_EINHALB = PI + PI_HALBE;
}
