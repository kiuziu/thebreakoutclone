package de.hpi.javaide.breakout.screens;

import java.awt.Rectangle;

import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.elements.ui.Buttons;
import de.hpi.javaide.breakout.elements.ui.InfoBox;
import de.hpi.javaide.breakout.starter.Game;

/**
 * The Screen can be in three states, either the StartScreen, the GameScreen, or
 * the EndScreen. The game logic takes care, which of those is the currently
 * active screen.
 * 
 * @author Ralf Teusner and Tom Staubitz
 *
 */
public class StartScreen implements Screen {

	/**
	 * This variable is needed for the Singleton pattern
	 */
	private static Screen instance;
	/**
	 * A reference to get access to the Processing features
	 */
	private Game game;

	private UIObject infoBox;

	/**
	 * it´s a switch to show the highscore list and at the other hand show the
	 * start menu
	 */
	private boolean toggleHighscore;

	private StartScreen(Game game) {
		this.game = game;
		init();
	}

	/**
	 * StartScreen implements a "Lazy Instantiation" of the Singleton Design
	 * Patterns (Gang of Four) This approach is not "Thread safe", but is
	 * sufficient for our current needs.
	 * 
	 * Please, be aware that Singletons need to be handled with care. There are
	 * various ways to implement them, all have there pros and cons. In his
	 * book, Effective Java, Joshua Bloch recommends to create Singletons using
	 * an enum, which is a language concept that we have not discussed here so
	 * far. For those of you who want to go further we suggest to follow this
	 * recommendation at some point of time.
	 *
	 * @return the StartScreen
	 */
	public static Screen getInstance(Game game) {
		if (instance == null) {
			instance = new StartScreen(game);
		} else {
			instance.init();
		}
		return instance;
	}

	/*
	 * The user should be able to start the game here (by switching to the
	 * GameScreen.)
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hpi.javaide.breakout.screens.Screen#handleKeyPressed(java.lang.String)
	 */
	@Override
	public void init() {
		game.noLoop();
		game.background(200);
		String info = "T H E - B R E A K O U T - C L O N E\n\n";
		info += "  Press [Enter] to start the game!\n\n";
		info += "  push [H] to display the highscore\n";
		info += "  push [N] to input your Name\n\n";
		info += "  push [Q] to quit the game\n\n";
		info += "  push [D] for the Demo Mode";
		infoBox = new InfoBox(game, info);
		infoBox.display();
		Buttons.add(game, KEY_ENTER, "[Enter]", new Rectangle(323, 225, 379 - 323, 239 - 225));
		Buttons.add(game, KEY_QUIT, "[Q]", new Rectangle(344, 372, 364 - 344, 383 - 372));
		Buttons.add(game, KEY_NAME, "[N]", new Rectangle(333, 315, 352 - 333, 325 - 315));
		Buttons.add(game, KEY_HGIHSCORE, "[H]", new Rectangle(310, 285, 331 - 310, 296 - 285));
		Buttons.add(game, KEY_HGIHSCORE, "[H]", new Rectangle(320, 137, 343 - 320, 152 - 137));
		Buttons.add(game, KEY_DEMO_MODE, "[D]", new Rectangle(334, 430, 354 - 334, 443 - 430));
		game.loop();
	}

	@Override
	public void update() { // is not used in this class
	}

	@Override
	public void display() {
		if (toggleHighscore) {
			game.score.displayHighscoreList();
		} else {
			infoBox.display();
		}
	}

	@Override
	public void handleKeyPressed(String key) {
		switch (key) {
		case Screen.KEY_ENTER:
			game.score.reset();
			ScreenManager.setScreen(game, Screen.GAME);
			break;
		case Screen.KEY_QUIT:
			System.exit(0);
			break;
		case Screen.KEY_NAME:
			game.score.setPlayersName();
			break;
		case Screen.KEY_HGIHSCORE:
			toggleHighscore = !toggleHighscore;
			game.draw();
			break;
		case Screen.KEY_DEMO_MODE:
			game.demoMode = true;
			ScreenManager.setScreen(game, Screen.GAME);
			break;
		default:
			break;
		}
	}

	@Override
	public void handleMouseDragged() {
	}

	@Override
	public void handleMouseButtonPressed() {

		switch (Buttons.clicked(game.getMousePosition())) {
		case KEY_ENTER:
			game.score.reset();
			ScreenManager.setScreen(game, Screen.GAME);
			break;
		case KEY_QUIT:
			System.exit(0);
			break;
		case KEY_NAME:
			game.score.setPlayersName();
			break;
		case KEY_HGIHSCORE:
			toggleHighscore = !toggleHighscore;
			game.draw();
			break;
		case KEY_DEMO_MODE:
			game.demoMode = true;
			ScreenManager.setScreen(game, Screen.GAME);
			break;
		default:
			break;
		}

	}

	@Override
	public void increaseScore(int i) {
		// Im StartScreen gibt es keinen Counter.
	}
}
