# README #

openHPI MOOC Einführung in eine IDE
-------------------------------------------

zum openHPI Kurs 'Einführung in eine IDE' mit Eclipse und Java ist eine Hausaufgabe zu lösen.
Ein bereits durch das openHPI Team erstelltes Breakout-Spiel soll durch die Kursteilnehmer einzeln oder in Teams zu Ende programmiert werden. Die Struktur und die Klassen und Interfaces sind (fast) fertig. Die fehlenden Methoden und Attribute sowie das Aussehen auf dem Bildschirm also das GUI (Grafik User Interface) soll frei durch die Kursteilnehmer/innen modelliert und gestaltet werden. Einzige Vorgabe ist eben es soll ein Breakout-Game erkennbar sein und es soll fehlerfrei und konsistent laufen.

Diese Implementierung läuft im im Windows-Fenster.
Folgen Sie den Anweisungen auf dem Bildschirm.

Das Spiel ist nun auch mit der Maus steuerbar. Einfach in die eckigen Klammern klicken.

Befehle auf der Startseite (Start-Ansicht):
- [Enter]-Taste Spiel starten. 
- [H]-Taste Highscore anzeigen/ausblenden.
- [N]-Taste Namen des Spielers/der Spielerin eingeben.
- [D]-Taste im Demo-Modus starten.
- [Q]-Taste (für Quit) um das Spiel zu beenden.

Was in der Game-Ansicht zusehen ist:
Neben Deinen Punkten wird auch die höchste Punktezahl, die Du mit einem Brick-Treffer erzielt hast angezeigt (Highest Hit). Außerdem gibt es weitere Infos zu den Faktoren, die in die Punkte-Berechnung eingehen.

- mit der [Enter]-Taste einen Ball holen. Der fällt aus der Mitte des Fensters.
- einfach mit der Maus das Paddle bewegen. Eine Taste muss nicht gedrückt werden.
- wenn der Ball aus geht mit der [Enter]-Taste den nächsten Ball holen.
- Achtung! Wenn du alle Steine aus der Mauer abgeschossen hast erscheint eine neu Wand.
- Das Spiel ist beendet, wenn alle Bälle ins Ausgegangen sind. Dein Name, deine erreichten Punkte und dein highest 
  Hit werden in der Highscore-Liste gespeichert.
  Bitte beachte: Es werden die Besten 10 Ergebnisse in der Highscore gespeichert.
  
Der Ende-Screen (Game-Over):
- Anzeige der aktualisiert Highscore-Liste. Deine Punkte und dein highest Hit.
- [Enter]-Taste um das Spiel erneut zu spielen. (restart)
- [Q]-Taste (für Quit) um das Spiel zu beenden.